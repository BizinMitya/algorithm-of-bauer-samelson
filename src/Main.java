import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Dmitriy on 12.02.2016.
 */
public class Main {
    //(5*(3+4)-(((7-2)*4-5*(4+2)+2)+1*7))=36
    //Отрицательные числа надо вводить так: 0-a, где a>0
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                Algorithm algorithm = new Algorithm();
                System.out.print("Введите выражение: ");
                StringBuffer expression = new StringBuffer(bufferedReader.readLine());
                Pattern p = Pattern.compile("^[0-9-+*/().]*$");
                Matcher m = p.matcher(expression);
                if (m.matches()) {
                    expression.append("$");
                    while (expression.length() != 0) {
                        if (expression.charAt(0) == '(' || expression.charAt(0) == '+' || expression.charAt(0) == '*' || expression.charAt(0) == '/' || expression.charAt(0) == ')' || expression.charAt(0) == '-' || expression.charAt(0) == '$') {
                            switch (algorithm.numberOfFunction(algorithm.operationToInt(algorithm.getOperation()), algorithm.operationToInt(expression.charAt(0)))) {
                                case 1: {
                                    algorithm.f1(expression.charAt(0));
                                    break;
                                }
                                case 2: {
                                    algorithm.f2(expression.charAt(0));
                                    break;
                                }
                                case 3: {
                                    algorithm.f3();
                                    break;
                                }
                                case 4: {
                                    algorithm.f4(expression.charAt(0));
                                    break;
                                }
                                case 5: {
                                    algorithm.f5();
                                    break;
                                }
                                case 6: {
                                    //algorithm.f5();
                                    break;
                                }
                            }
                            expression.delete(0, 1);
                        } else {
                            StringBuffer number = new StringBuffer();
                            int i;
                            for (i = 0; i < expression.length(); i++) {
                                if (Character.isDigit(expression.charAt(i)) || expression.charAt(i) == '.') {
                                    number.append(expression.charAt(i));
                                } else {
                                    break;
                                }
                            }
                            expression.delete(0, i);
                            algorithm.addOperand(Double.parseDouble(number.toString()));
                        }
                    }
                    if (!algorithm.isError()) {
                        System.out.println("Результат выражения: " + algorithm.getResult());
                    } else {
                        System.out.println("Во время вычисления выражения произошла ошибка!Проверьте правильность выражения!");
                    }
                } else {
                    System.out.println("В выражении содержатся некорректные символы!");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Неправильно введено число!");
            e.printStackTrace();
        }
    }
}
