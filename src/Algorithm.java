import java.util.Arrays;
import java.util.Stack;

/**
 * Created by Dmitriy on 12.02.2016.
 */
public class Algorithm {
    private Stack<Character> operations;
    private Stack<Double> operands;
    private int[][] table;
    private boolean error = false;

    public Algorithm() {
        operations = new Stack<>();
        operations.push('$');
        operands = new Stack<>();
        table = new int[6][7];
        Arrays.fill(table[0], 1);
        Arrays.fill(table[1], 1);
        Arrays.fill(table[2], 1);
        Arrays.fill(table[3], 1);
        Arrays.fill(table[4], 1);
        Arrays.fill(table[5], 1);
        table[0][0] = 6;
        table[0][6] = 5;
        table[1][0] = 5;
        table[1][6] = 3;
        table[2][0] = 4;
        table[2][2] = 2;
        table[2][3] = 2;
        table[2][6] = 4;
        table[3][0] = 4;
        table[3][2] = 2;
        table[3][3] = 2;
        table[3][6] = 4;
        table[4][0] = 4;
        table[4][2] = 4;
        table[4][3] = 4;
        table[4][4] = 2;
        table[4][5] = 2;
        table[4][6] = 4;
        table[5][0] = 4;
        table[5][2] = 4;
        table[5][3] = 4;
        table[5][4] = 2;
        table[5][5] = 2;
        table[5][6] = 4;
        /*

          $ ( + - * / )
        $ 6 1 1 1 1 1 5
        ( 5 1 1 1 1 1 3
        + 4 1 2 2 1 1 4
        - 4 1 2 2 1 1 4
        * 4 1 4 4 2 2 4
        / 4 1 4 4 2 2 4

        $ -> 0
        ( -> 1
        + -> 2
        - -> 3
        * -> 4
        / -> 5
        ) -> 6

        */

        /*for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }*/
    }

    public void f1(char operation) {
        operations.push(operation);
    }

    public void f2(char operation) {
        char op = operations.pop();
        double number1 = operands.pop();
        double number2 = operands.pop();
        switch (op) {
            case '+': {
                operands.push(number1 + number2);
                break;
            }
            case '-': {
                operands.push(number2 - number1);
                break;
            }
            case '*': {
                operands.push(number1 * number2);
                break;
            }
            case '/': {
                if (number1 == 0 || number2 == 0) {
                    f5();
                } else {
                    operands.push(number2 / number1);
                }
                break;
            }
        }
        operations.push(operation);
    }

    public void f3() {
        operations.pop();
    }

    public int operationToInt(char operation) {
        int result = 7;
        switch (operation) {
            case '$': {
                result = 0;
                break;
            }
            case '(': {
                result = 1;
                break;
            }
            case '+': {
                result = 2;
                break;
            }
            case '-': {
                result = 3;
                break;
            }
            case '*': {
                result = 4;
                break;
            }
            case '/': {
                result = 5;
                break;
            }
            case ')': {
                result = 6;
                break;
            }
        }
        return result;
    }

    public void f4(char operation) {
        char op = operations.pop();
        double number1 = operands.pop();
        double number2 = operands.pop();
        switch (op) {
            case '+': {
                operands.push(number1 + number2);
                break;
            }
            case '-': {
                operands.push(number2 - number1);
                break;
            }
            case '*': {
                operands.push(number1 * number2);
                break;
            }
            case '/': {
                if (number1 == 0 || number2 == 0) {
                    f5();
                } else {
                    operands.push(number2 / number1);
                }
                break;
            }
        }
        int i = 0, j = 0;
        i = operationToInt(operations.peek());
        j = operationToInt(operation);
        switch (table[i][j]) {
            case 1: {
                f1(operation);
                break;
            }
            case 2: {
                f2(operation);
                break;
            }
            case 3: {
                f3();
                break;
            }
            case 4: {
                f4(operation);
                break;
            }
            case 5: {
                break;
            }
            case 6: {
                break;
            }
        }
    }

    public int numberOfFunction(int i, int j) {
        return table[i][j];
    }

    public void addOperand(double number) {
        operands.push(number);
    }

    public char getOperation() {
        return operations.peek();
    }

    public double getResult() {
        return operands.pop();
    }

    public void f5() {
        error = true;
    }

    public void f6() {
    }

    public boolean isError() {
        return error;
    }
}
